const browserSync = require('browser-sync').create();
const del= require('del');
const gulp = require('gulp');
const csso = require('gulp-csso');
const imagemin = require('gulp-imagemin');
const rename = require('gulp-rename');
const sass = require('gulp-sass');


gulp.task('html', () => {
    return gulp.src(['src/html/*.html'])
        .pipe(gulp.dest('build/'));
});

gulp.task('normalize.css', () => {
    return gulp.src('node_modules/normalize.css/normalize.css')
        .pipe(gulp.dest('src/css/'));
});

gulp.task('sass', () => {
    return gulp.src('src/scss/styles.{sass,scss}')
        .pipe(sass())
        .pipe(gulp.dest('src/css/'));
});

gulp.task('csso', () => {
    return gulp.src('src/css/*.css')
        .pipe(csso({
            comments: false
        }))
        .pipe(rename(path => {
            path.basename += '.min';
        }))
        .pipe(gulp.dest('build/css/'))
        .pipe(browserSync.stream());
});

gulp.task('imgo', () => {
    return gulp.src('src/images/**/*.{jpg,png,svg}')
        .pipe(imagemin([
            imagemin.optipng({optimizationLevel: 3}),
            imagemin.jpegtran({progressive: true}),
            imagemin.svgo()
        ]))
        .pipe(gulp.dest('src/images/'));
});

gulp.task('clean', () => {
    return del('build/*');
});

gulp.task('copy', () => {
    return gulp.src([
            'src/fonts/*.{woff,woff2}',
            'src/images/**'
        ], {
            base: 'src'
        })
        .pipe(gulp.dest('build/'));
});

gulp.task('build', gulp.series('clean', 'html', 'normalize.css', 'sass', 'csso', 'copy'));

gulp.task('serve', () => {
    browserSync.init({
        server: {
            baseDir: 'build/'
        },
        browser: 'chrome'
    });

    gulp.watch('src/scss/**/*.{sass,scss}', gulp.series('sass', 'csso'));

    gulp.watch(
        'src/html/*',
        gulp.series('html', done => {
            browserSync.reload();
            done();
        })
    );

    gulp.watch([
            'src/fonts/*.{woff,woff2}',
            'src/images/**'
        ],
        gulp.series('copy')
    );
});
